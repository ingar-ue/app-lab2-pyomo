# Flask API & Pyomo + GLPK

Ejecutar app.py activa el server Flask que responde a POST Requests en 0.0.0.0:8083/op1.

La request debe incluir un JSON de formato:

```
{
"markets":
            [{"name":"new-york","demand":325},{"name":"chicago","demand":300},{"name":"topeka","demand":275}],
"plants":
            [{"name":"seattle","productiveCapacity":350},{"name":"san-diego","productiveCapacity":600}],
"distances":
            [{"marketName":"new-york","plantName":"seattle","distance":2.5},{"marketName":"new-york","plantName":"san-diego","distance":2.5},{"marketName":"chicago","plantName":"seattle","distance":1.7},{"marketName":"topeka","plantName":"seattle","distance":1.8},{"marketName":"chicago","plantName":"san-diego","distance":1.8},{"marketName":"topeka","plantName":"san-diego","distance":1.4}],
"freight":90
}
```

Que es pasado a run.py, inicializa las estructuras de datos de Pyomo,
ejecuta el solver GLPK y retorna una respuesta HTTP con un JSON de resultado en formato:

```
[
    [
        {
            "mercado": "chicago",
            "planta": "san-diego",
            "valor": 0
        },
        {
            "mercado": "topeka",
            "planta": "san-diego",
            "valor": 275
        },
        {
            "mercado": "new-york",
            "planta": "san-diego",
            "valor": 325
        },
        {
            "mercado": "chicago",
            "planta": "seattle",
            "valor": 300
        },
        {
            "mercado": "topeka",
            "planta": "seattle",
            "valor": 0
        },
        {
            "mercado": "new-york",
            "planta": "seattle",
            "valor": 0
        }
    ],
    [
        "objective",
        153.67499999999998
    ]
]
```

## Conda Enviroment Setup

```
conda create --name <nombreEntorno>
conda install -c conda-forge pyomo
conda install -c conda-forge pyomo.extras
conda install -c conda-forge glpk
conda install -c conda-forge flask
conda install -c conda-forge flask-cors
conda install -c conda-forge waitress
```

## Servicios y ejecución en el servidor

Para correr como servicio una aplicación de python en un entorno determinado se debe generar un archivo .bat en el mismo directorio que la aplicación a ejecutar:

```
@echo on
CALL C:\<path>\Anaconda3\Scripts\activate.bat <nombreEntorno>
python app.py
pause
```

Donde < path > refiere a la raíz de la instalación de Anaconda y < nombreEntorno > al entorno a utilizar.

La gestión de servicios puede realizarse con NSSM (https://nssm.cc/):

```
nssm install <nombreServicio>
```

Abre una UI que permite configurar todos los parametros, mas información en la documentación. (https://nssm.cc/usage)

Los servicios pueden gestionarse vía services.msc (Win + R -> services.msc)

## Main Thread Error - Signal Handling

Referencia para solución de errores a la hora de correr modelos de Pyomo ejecutados por un servidor Flask:

https://github.com/PyUtilib/pyutilib/issues/31

## Build & Deploy at INGAR

- Login at Server windows (conda env prod1Env)
```
cd c:\code\app-lab2-pyomo
git pull origin master
```
- Kill if running, remove logs, copy new version, and restart
```
copy C:\code\app-lab2-pyomo\*.py C:\apps\app-lab2-py\
copy C:\code\app-lab2-pyomo\start-app-lab2-py.bat C:\apps\app-lab2-py\
cd C:\apps\app-lab2-py
C:\apps\app-lab2-py\start-app-lab2-py.bat
```
- del C:\apps\app-lab2-py\app-lab2.log

### view live logs en powershell
```
view live logs en powershell: Get-Content -Path "C:\apps\app-lab2-py\app-lab2.log" -Wait
```
