from flask import Flask
from flask import request, jsonify
from flask_cors import CORS
from optimization import runOptimizationPoint
from waitress import serve
import time

app = Flask(__name__)
CORS(app)

@app.route('/op1', methods=['POST'])
def postJsonHandler():
    start = time.strftime("%c")
    print("Executing optimizer: " + start)
    # print(request.is_json)
    content = request.get_json()

    # for i in range(len(content["markets"])):
    #     print(content["markets"][i]["name"])
    # print(content["plants"])
    # print(content["distances"])
    # print(content["freight"])

    resultdata = runOptimizationPoint(content)
    resp = jsonify(resultdata)

    resp.status_code = 200
    return resp


if __name__ == "__main__":
    print("Starting app at " + time.strftime("%c"))
    # serve(app, host='0.0.0.0', port=8083)
    serve(app, listen = '*:8083')
