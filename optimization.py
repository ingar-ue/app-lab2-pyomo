# -*- coding: utf-8 -*-
import subprocess
from flask import jsonify
from pyomo.environ import *
from pyomo.opt import TerminationCondition
from pyomo.opt import SolverFactory
import pyutilib.subprocess.GlobalData


class OptModel:

    def __init__(self, jsonInput):
        # pre process jsonInput and generate data
        self.createModel(jsonInput)

    def createModel(self, jsonInput):
        self.model = ConcreteModel()

        # Variable Initialization
        plants = []
        markets = []
        capacity = {}
        demand = {}
        distance = {}
        cost = 0

        # Variable Asignations
        for i in range(len(jsonInput["plants"])):
            plants.append(jsonInput["plants"][i]["name"])
        #print(plants)

        for i in range(len(jsonInput["markets"])):
            markets.append(jsonInput["markets"][i]["name"])
        #print(markets)

        for i in range(len(jsonInput["plants"])):
            capacity[jsonInput["plants"][i]["name"]] = jsonInput["plants"][i]["productiveCapacity"]
        #print(capacity)

        for i in range(len(jsonInput["markets"])):
            demand[jsonInput["markets"][i]["name"]] = jsonInput["markets"][i]["demand"]
        #print(demand)

        for i in range(len(jsonInput["distances"])):
            tupla = (jsonInput["distances"][i]["plantName"], jsonInput["distances"][i]["marketName"])
            distance[tupla] = jsonInput["distances"][i]["distance"]
        #print(distance)

        cost = jsonInput["freight"]

        # Define sets ##
        #  Sets
        #       i   canning plants   / seattle, san-diego /
        #       j   markets          / new-york, chicago, topeka / ;
        self.model.i = Set(initialize=plants, doc='Plantas de produccion')
        self.model.j = Set(initialize=markets, doc='Mercados')
        # Define parameters ##
        #   Parameters
        #       a(i)  capacity of plant i in cases
        #         /    seattle     350
        #              san-diego   600  /
        #       b(j)  demand at market j in cases
        #         /    new-york    325
        #              chicago     300
        #              topeka      275  / ;
        self.model.a = Param(self.model.i, initialize=capacity,
                             doc='Capacity of plant i in units')
        self.model.b = Param(self.model.j, initialize=demand,
                                     doc='Demand at market j in units')
        #  Table d(i,j)  distance in thousands of miles
        #                    new-york       chicago      topeka
        #      seattle          2.5           1.7          1.8
        #      san-diego        2.5           1.8          1.4  ;
        # dtab = {
        #     ('seattle', 'new-york'): 2.5,
        #     ('seattle', 'chicago'): 4.9,
        #     ('seattle', 'topeka'): 2.4,
        #     ('san-diego', 'new-york'): 2.5,
        #     ('san-diego', 'chicago'): 3.2,
        #     ('san-diego', 'topeka'): 1.7,
        # }
        self.model.d = Param(self.model.i, self.model.j, initialize=distance, doc='Distancia en km')
        #  Scalar f  freight in dollars per case per thousand miles  /90/ ;
        self.model.f = Param(initialize=cost, doc='Costo de transporte en pesos por unidad por km')

        #  Parameter c(i,j)  transport cost in thousands of dollars per case ;
        #            c(i,j) = f * d(i,j) / 1000 ;
        def c_init(model, i, j):
            return model.f * model.d[i, j] / 1000

        self.model.c = Param(self.model.i, self.model.j, initialize=c_init, doc='Transport cost in thousands of dollar per case')
        # Define variables ##
        #  Variables
        #       x(i,j)  shipment quantities in cases
        #       z       total transportation costs in thousands of dollars ;
        #  Positive Variable x ;
        self.model.x = Var(self.model.i, self.model.j, bounds=(0.0, None), doc='Shipment quantities in case')

        # Define contrains ##
        # supply(i)   observe supply limit at plant i
        # supply(i) .. sum (j, x(i,j)) =l= a(i)
        def supply_rule(model, i):
            return sum(model.x[i, j] for j in model.j) <= model.a[i]

        self.model.supply = Constraint(self.model.i, rule=supply_rule, doc='Observe supply limit at plant i')

        # demand(j)   satisfy demand at market j ;
        # demand(j) .. sum(i, x(i,j)) =g= b(j);
        def demand_rule(model, j):
            return sum(model.x[i, j] for i in model.i) >= model.b[j]

        self.model.demand = Constraint(self.model.j, rule=demand_rule, doc='Satisfy demand at market j')

        # Define Objective and solve ##
        #  cost        define objective function
        #  cost ..        z  =e=  sum((i,j), c(i,j)*x(i,j)) ;
        #  Model transport /all/ ;
        #  Solve transport using lp minimizing z ;

        def objective_rule(model):
            return sum(model.c[h, k] * model.x[h, k] for h in model.i for k in model.j)

        self.model.objective = Objective(rule=objective_rule, sense=minimize, doc='Define objective function')

    # Display of the output
    # Display x.l, x.m ;
    def solve(self):
        print("Solve start - set SIGNAL_HANDLERS_DEFAULT false")
        # Avoid multithreading (locks glpk execution)
        pyutilib.subprocess.GlobalData.DEFINE_SIGNAL_HANDLERS_DEFAULT = False

        print("Solve start - set solver glpk")
        opt = SolverFactory("glpk")
        # opt.use_signal_handling = None
        print("Solve start - call solve")
        results = opt.solve(self.model)

        #print(dir(self))

        fullarray = []

        #print("Variable values:")
        for p in self.model.x:
            element = {}
            #print(p)
            #print(self.model.x[p].value)
            element['planta'] = p[0]
            element['mercado'] = p[1]
            element['valor'] = self.model.x[p].value
            fullarray.append(element)

        #print(self.model.objective())

        objectivetuple = ('objective', self.model.objective())

        answer = (fullarray, objectivetuple)

        #print(fullarray)

        return answer
        # This is an optional code path that allows the script to be run outside of
        # pyomo command-line.  For example:  python transport.py


# if __name__ == '__main__':
def runOptimizationPoint(request):

    modelo = OptModel(request)

    res = modelo.solve()
    return res